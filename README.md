## welcome to the chalakas home of all repos.

**Hello and welcome to my repository head quarters. this is where you will find all the repos I have built over the years**

As I write this, I have repos on at least 12 different technologies. I wish it was just one technology but the way the tech world is going it is almost impossible these days to be working on a single tech. Everything is so complicated and we developers have no choice but to jack of all trades. 

---

Here are the links to the repo homes where you will find what you are looking for.

 1. [C language](https://bitbucket.org/thechalakas/repohomeclanguage/src/master/)
 2. [C sharp stuff](https://bitbucket.org/thechalakas/repohomecsharpstuff/src/master/)
 3. [dot net core](https://bitbucket.org/thechalakas/repohomedotnetcore/src/master/)
 4. [droid stuff](https://bitbucket.org/thechalakas/repohomedroidstuff/src/master/)
 5. [iOS stuff](https://bitbucket.org/thechalakas/repohomeiosstuff/src/master/)
 6. [other google](https://bitbucket.org/thechalakas/repohomeothergoogle/src/master/)
 7. [project TD](https://bitbucket.org/thechalakas/repohomeprojecttd/src/master/)
 8. [project UM](https://bitbucket.org/thechalakas/repohomeprojectum/src/master/)
 9. [react native](https://bitbucket.org/thechalakas/repohomereactnative/src/master/)
10. [asp.net MVC](https://bitbucket.org/thechalakas/repohomewebmvcaspdotnet/src/master/)
11. [JavaScript](https://bitbucket.org/thechalakas/repohomewebjavascript/src/master/)
12. [jQuery](https://bitbucket.org/thechalakas/repohomewebjquery/src/master/)

---

## additional notes here.

Last Updated June 10th 2018.

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 